@IDAModel @5.7 @IDA
Feature: Ignite Defect Analytics <att_name>
Scenario: IDA_Regression_1
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Application]" button
And I enter into input field "[Application Name Searched]" the value "(Application Name Searched)"
And I click on "[Global_Bank]" button
And I add wait seconds of "5"
And "[Validating Application Name and Domain]" should have text as "(Validating Application Name and Domain)"
And I click on "[Defect Analysis ]" button
And "[Classification Results]" should have text as "Classification Results"
And I click on "[Ok]" button
And I click on "[Upload]" button
And I click on "[Excel Validation]" button
And I click on "[Upload]" button
And I click on "[Upload File ]" button
And I upload to "[IDA Data for Upload]" the file "C:\IGNITE\GlobalBank\GlobalBank_Release5IDA.xlsx"
And I click on "[Submit]" button
And I click on "[BackButton]" button
And I click on "[Upload]" button
And I click on "[Upload File ]" button
And I upload to "[IDA Defect Data Upload]" the file "C:\IGNITE\GlobalBank\GlobalBank_Release5IDA.xlsx"
And I click on "[Submit]" button
And I add wait seconds of "10"
And "[Upload Defect Information]" should have text as "Upload Defect Information"
And I click on "[Ok]" button
And I click on "[Data]" button
And I add wait seconds of "300"
And I click on "[Dashboard]" button
And I add wait seconds of "20"
And I click on "[Prevent]" button
And I click on "[Click Edit Button]" button
And I click on "[Click Save]" button
And I click on "[Click button Publish to IGNITE]" button
And "[Confirm Publish Recommendation]" should have text as "Confirm Publish Recommendation"
And I click on "[Click Proceed]" button
And "[Success]" should have text as "Success"
And I click on "[Ok]" button
And I click on "[Invoke Prevent]" button

Scenario: IDA_Regression_2
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Application]" button
And I enter into input field "[Application Name Searched]" the value "(Application Name Searched)"
And I click on "[Global_Bank]" button
And I add wait seconds of "5"
And "[Validating Application Name and Domain]" should have text as "(Validating Application Name and Domain)"
And I click on "[Defect Analysis ]" button
And "[Classification Results]" should have text as "Classification Results"
And I click on "[Ok]" button
And I click on "[Upload]" button
And I click on "[Excel Validation]" button
And I click on "[Upload]" button
And I click on "[Upload File ]" button
And I upload to "[IDA Data for Upload]" the file "C:\IGNITE\GlobalBank\GlobalBank_Release5IDA.xlsx"
And I click on "[Submit]" button
And I click on "[BackButton]" button
And I click on "[Upload]" button
And I click on "[Upload File ]" button
And I upload to "[IDA Defect Data Upload]" the file "C:\IGNITE\GlobalBank\GlobalBank_Release5IDA.xlsx"
And I click on "[Submit]" button
And I add wait seconds of "10"
And "[Upload Defect Information]" should have text as "Upload Defect Information"
And I click on "[Ok]" button
And I click on "[Data]" button
And I add wait seconds of "300"
And I click on "[Dashboard]" button
And I add wait seconds of "20"
And I click on "[Prevent]" button
And I click on "[Click Edit Button]" button
And I click on "[Click Save]" button
And I click on "[Click button Publish to IGNITE]" button
And "[Confirm Publish Recommendation]" should have text as "Confirm Publish Recommendation"
And I click on "[Click Proceed]" button
And "[Success]" should have text as "Success"
And I click on "[Ok]" button
And I click on "[Invoke Prevent]" button

