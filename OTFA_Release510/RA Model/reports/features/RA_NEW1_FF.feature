@RA_New_Model @IGNITE_5.7 @Requirement_Analysis
Feature: Requirement Analysis Add
Scenario: RA_Flow_1
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_2
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_3
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_4
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_5
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_6
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_7
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_8
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_9
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_10
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_11
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_12
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_13
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_14
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_15
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_16
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_17
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

Scenario: RA_Flow_18
Given I navigate to "https://9.121.56.114:8443/ignitePlatform/"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
And I click on "[SignIn]" button
And I add wait seconds of "5"
And I click on "[Project]" button
And I enter into input field "[ProjectNameSearch]" the value "(ProjectNameSearch)"
And I click on "[selectingSearchedProject]" button
And I click on "[GlobalBank]" button
And I add wait seconds of "5"
And "[Validating Application and Project Name]" should have text as "(Validating Application and Project Name)"
And I click on "[Analyze Requirements]" button
And I click on "[Add Requirements]" button
And "[Create Requirement]" should have text as "Create Requirement"
And I enter into input field "[Document Name]" the value "(Document Name)"
And I enter into input field "[Requirement ID]" the value "(Requirement ID)"
And I enter into input field "[Statement]" the value "(Statement)"
And I click on "[Create]" button
And I check the checkbox "[Select Checkbox click for Edit Requirement]"
And I click on "[Edit Requirement]" button
And I clear the content of input field "[Document Name]"
And I enter into input field "[Document Name]" the value "(Edit Document Name)"
And I clear the content of input field "[Requirement ID]"
And I enter into input field "[Requirement ID]" the value "(Edit Requirement ID)"
And I clear the content of input field "[Statement]"
And I enter into input field "[Statement]" the value "(Edit Statement)"
And I click on "[Update]" button
And I check the checkbox "[Select Checkbox click to Delete Requirement]"
And I check the checkbox "[Click Delete]"
And I click on "[Click Yes]" button
And I click on "[Upload]" button
And I upload to "[RA Data for Uplaod]" the file "C:\IGNITE\GlobalBank\Global Bank_Release5RA.xlsx"
And I add wait seconds of "5"
And "[File uploaded Successfully]" should have text as "File uploaded Successfully"
And I click on "[Click OK]" button
And I click on "[Click Download]" button
Then "[Choose how you want to download]" should have text as "Choose how you want to download"
And "[EXPORT TO FOCUS]" should have text as "EXPORT TO FOCUS"
And "[EXPORT AS EXCEL]" should have text as "EXPORT AS EXCEL"
And I click on "[Cancel]" button
And I click on "[Calculate Complexity & Ranking]" button
And I add wait seconds of "10"
And I click on "[View Related Requirement]" button
And "[EXPORT TO EXCEL]" should have text as "EXPORT TO EXCEL"
And "[EXPORT AS PDF]" should have text as "EXPORT AS PDF"
And I click on "[Click Close Icon]" button

